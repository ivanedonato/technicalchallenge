# AccountManagementService::Personnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_id** | **String** | Person ID of the personnel | [optional] 
**first_name** | **String** | Firstname of the personnel | [optional] 
**last_name** | **String** | Lastname of the personnel | [optional] 
**date_education_to_be_completed** | **String** | Date education session must be completed by | [optional] 
**restriction_type** | **String** | Restriction type | [optional] 


