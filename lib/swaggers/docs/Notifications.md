# AccountManagementService::Notifications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notification** | **String** | Notification of matters affecting approval | [optional] 
**notification_date** | **String** | Date of the notification | [optional] 


