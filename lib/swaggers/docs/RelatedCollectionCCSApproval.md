# AccountManagementService::RelatedCollectionCCSApproval

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;CCSApproval&gt;**](CCSApproval.md) |  | [optional] 


