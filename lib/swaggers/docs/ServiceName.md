# AccountManagementService::ServiceName

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the Service | [optional] 
**startdate** | **String** | Validity start date of of this service | [optional] 
**enddate** | **String** | Validity end date of this Service name | [optional] 


