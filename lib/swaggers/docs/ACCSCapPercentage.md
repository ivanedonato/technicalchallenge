# AccountManagementService::ACCSCapPercentage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **String** | Start Date of ACCS CWB cap percentage | [optional] 
**end_date** | **String** | End Date of ACCS CWB cap percentage | [optional] 
**percentage** | **String** | ACCS Child Wellbeing cap percentage | [optional] 
**supporting_information** | **String** | Supporting information for this request | [optional] 
**reason** | **String** | Reason for this change request | [optional] 
**agencies** | [**RelatedCollectionAgency**](RelatedCollectionAgency.md) |  | [optional] 


