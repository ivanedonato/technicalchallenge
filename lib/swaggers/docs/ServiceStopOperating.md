# AccountManagementService::ServiceStopOperating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expected_closure_date** | **String** | Anticipated date of closure of the service | [optional] 
**is_sale** | **BOOLEAN** | TRUE | [optional] 
**parents_notified** | **BOOLEAN** | TRUE | [optional] 
**notify_dept** | **BOOLEAN** | TRUE | [optional] 
**service_stop_operating_sale** | [**RelatedCollectionServiceStopOperatingSale**](RelatedCollectionServiceStopOperatingSale.md) |  | [optional] 


