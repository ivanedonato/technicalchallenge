# AccountManagementService::ChildCarePlaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number_offered** | **Integer** | Amount of Places offered by the Service | [optional] 
**number_offered_date_of_event** | **String** | Date of event for the update | [optional] 


