# AccountManagementService::Fee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique ID for fee | [optional] 
**description** | **String** | Description for fee | [optional] 
**amount** | **String** | FeeAmount | [optional] 
**unit_of_measure** | **String** | Rate of charge for the fee | [optional] 
**action** | **String** | Action for Fee | [optional] 


