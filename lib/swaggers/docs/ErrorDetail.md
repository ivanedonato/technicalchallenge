# AccountManagementService::ErrorDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | 
**message** | **String** |  | [optional] 
**target** | **String** |  | [optional] 


