# AccountManagementService::RelatedCollectionFee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;Fee&gt;**](Fee.md) |  | [optional] 


