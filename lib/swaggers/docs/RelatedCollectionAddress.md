# AccountManagementService::RelatedCollectionAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;Address&gt;**](Address.md) |  | [optional] 


