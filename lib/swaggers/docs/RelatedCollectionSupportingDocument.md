# AccountManagementService::RelatedCollectionSupportingDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;SupportingDocument&gt;**](SupportingDocument.md) |  | [optional] 


