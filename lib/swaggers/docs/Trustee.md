# AccountManagementService::Trustee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **String** | Validity start date of this Service Trustee relationship | [optional] 
**end_date** | **String** | Validity end date of this Service Trustee relationship | [optional] 
**action** | **String** | Action for this Trustee | [optional] 
**abn** | **String** | ABN of the Trustee | [optional] 
**name** | **String** | Name of the Trustee | [optional] 


