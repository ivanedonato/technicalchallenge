# AccountManagementService::Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | 
**message** | **String** |  | [optional] 
**error_details** | [**Array&lt;ErrorDetail&gt;**](ErrorDetail.md) |  | [optional] 


